<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
  <h1>OCR</h1>
  <form method="post" action="${contextPath}/upload" enctype="multipart/form-data" id="fileUploadForm">
    <input type='file' name='data' />
    <input type="submit" value="업로드" id="btnSubmit"/>
  </form>
  <textarea rows="10" cols="30" id="tarea"></textarea>
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script>
    $("#btnSubmit").click(function (event) {
      event.preventDefault();
      var form = $('#fileUploadForm')[0];
      var data = new FormData(form);
      $("#btnSubmit").prop("disabled", true);

      $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/upload",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
          $("#btnSubmit").prop("disabled", false);
          textarea.value=data.response;
        },
        error: function (e) {
          console.log("ERROR : ", e);
            $("#btnSubmit").prop("disabled", false);
            alert("fail");
        }
      });
    });
	</script>
</body>
</html>