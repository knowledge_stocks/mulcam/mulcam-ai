<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<button id="record">녹음</button>
	<button id="stop">정지</button>
	<br>
	<textarea rows="10" cols="30" id="tarea"></textarea>
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script>
        const record = document.getElementById("record");
        const stop = document.getElementById("stop");
        const textarea = document.getElementById("tarea");
        // 오디오 컨텍스트 정의
        const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
        //주파수를 시각화 할 수 있는 AnalyserNode생성
        const analyser = audioCtx.createAnalyser();
		
        //카메라, 마이크, 화면 공유와 같이 현재 연결된 미디어 입력 장치에 접근할 수 있는 MediaDevices 객체를 반환
        if (navigator.mediaDevices) {
            console.log('getUserMedia supported.')

            const constraints = {
            	//오디오를 요청한다.
                audio: true
            }
            let chunks = [];

            navigator.mediaDevices.getUserMedia(constraints)
                .then(stream => {
                	//스트림 사용
                    const mediaRecorder = new MediaRecorder(stream);             
                    record.onclick = () => {
                        mediaRecorder.start();
                        console.log(mediaRecorder.state);
                        console.log("recorder started");
                        record.style.background = "red";
                        record.style.color = "black";
                    }

                    stop.onclick = () => {
                        mediaRecorder.stop();
                        console.log(mediaRecorder.state);
                        console.log("recorder stopped");
                        record.style.background = "";
                        record.style.color = "";
                    }

                    mediaRecorder.onstop = e => {
                    	//JavaScript에서 Blob(Binary Large Object, 블랍)은 이미지, 사운드, 비디오와 같은 멀티미디어 데이터를 다룰 때 사용
                    	// new Blob(array, options)
                    	// array :  ArrayBuffer, ArrayBufferView, Blob(File), DOMString 객체 또는 이러한 객체가 혼합된 Array를 사용
                        // options : type과 endings를 설정한다.
                        // type은 데이터의 MIME 타입을 설정하며, 기본값은 "" 입니다.
                        // endings는 \n을 포함하는 문자열 처리를 "transparent"와 "native"로 지정할 수 있으며, 기본값은 "transparent"입니다.
                    	const blob = new Blob(chunks, {
                    		//Opus의 주요 경쟁 코덱은 회사에서 쓰이는 영상회의, 인터넷 전화, 음성 채팅 등에 쓰이는 Speex, G.711, G.729 등의 VoIP용 코덱이다.
                    		//Opus 코덱의 목적은 실시간 인터넷 음성 통신과 같은 저비트레이트 상황에서의 극한의 효율성을 추구하며 개발된 포맷이기 때문에, 고비트레이트로 갈수록 음질의 효율성이 대단히 떨어진다
                            'type': 'audio/ogg codecs=opus'
                        });
                    	
                    	//file upload
                        let formdata = new FormData();
                        formdata.append("fname", "audio.webm");
                        formdata.append("data", blob);
                        

                        let xhr = new XMLHttpRequest();
                        xhr.onload = () => {
                         	if (xhr.status === 200) {// HTTP가 잘 동작되었다는 뜻.
	                        	console.log("response:"+xhr.response);
                        		textarea.value=xhr.response;       
                        	}                 
                        	
                        }
                        console.log('upload');
                        xhr.open("POST", "upload", true);
                        xhr.send(formdata);
                    }
 
                    mediaRecorder.ondataavailable = e => {
                        chunks.push(e.data) // 여기에서 cunks에 데이터를 넣었네
                    }
                })
                .catch(err => {
                	// 오류 처리
                    console.log('The following error occurred: ' + err)
                })
        }
    </script>
</body>

</html>