<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>       
<c:set var="contextPath" value="${pageContext.request.contextPath }"/>      
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>마이크 테스트</title>
<script src="https://code.jquery.com/jquery-latest.js"></script>
<script>
$(function() {
	$("#speaker").click(function() {
		//alert("button click");
		var request = new XMLHttpRequest();
		request.responseType = "blob";
		request.onload = function() {
 			var audioURL = URL.createObjectURL(this.response);
 			alert(audioURL);
 			var audio = new Audio();
			audio.src = audioURL;
			audio.play();
 		}
		request.open("POST", 'ttvoice');
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		request.send("textdata="+$("textarea#textstr").val()); 
	});
});

</script>
</head>
<body>
<textarea rows="10" cols="100" id="textstr">
    </textarea><br> 
    <button id="speaker" type="button">스피커</button>
    <div id="container"></div>
</body></html>
