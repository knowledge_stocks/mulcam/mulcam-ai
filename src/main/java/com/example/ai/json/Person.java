package com.example.ai.json;

import lombok.Data;

@Data
public class Person {
  private String name;
  private int age;
  private boolean success;
}
