package com.example.ai.json;

import com.google.gson.Gson;

public class Gson_03_object {
  public static void main(String[] args) {
    String json = "{\"name\":\"kim\",\"age\":20,\"success\":\"true\"}";

    Gson gson = new Gson();
    Person ps = gson.fromJson(json, Person.class);
    System.out.printf("name=%s, age=%d, success=%b\n", ps.getName(), ps.getAge(), ps.isSuccess());
  }
}
