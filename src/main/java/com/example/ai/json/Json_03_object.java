package com.example.ai.json;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Json_03_object {
  public static void main(String[] args) throws ParseException {
    String json = "{\"name\":\"kim\",\"age\":20,\"success\":\"true\"}";

    JSONParser parser = new JSONParser();
    JSONObject jsonObj = (JSONObject)parser.parse(json);
    System.out.println(jsonObj);

    String name = jsonObj.get("name").toString();
    int age = Integer.parseInt(jsonObj.get("age").toString());
    boolean success = Boolean.parseBoolean(jsonObj.get("success").toString());

    Person ps = new Person();
    ps.setName(name);
    ps.setAge(age);
    ps.setSuccess(success);
    System.out.printf("name=%s, age=%d, success=%b\n", ps.getName(), ps.getAge(), ps.isSuccess());
  }
}
