package com.example.ai.naver_clova.capcha;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Capcha03Result {
  public static void main(String[] args) {
    // X-NCP-APIGW-API-KEY-ID
    String clientId = "wo0d8tt658";
    // X-NCP-APIGW-API-KEY
    String clientSecret = "1V59Y5TdzPBx2cIUHJaQ0MGDH9t3SsyFuijHtNHN";
    try {
      String code = "1"; // 키 발급시 0,  캡차 이미지 비교시 1로 세팅
      String key = "YdySbsZkC1hhMYmz"; // 캡차 키 발급시 받은 키값
      String value = "CX75"; // 사용자가 입력한 캡차 이미지 글자값
      String apiURL = "https://naveropenapi.apigw.ntruss.com/captcha/v1/nkey?code=" + code +"&key="+ key + "&value="+ value;

      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection)url.openConnection();
      con.setRequestMethod("GET");
      con.setRequestProperty("X-NCP-APIGW-API-KEY-ID", clientId);
      con.setRequestProperty("X-NCP-APIGW-API-KEY", clientSecret);
      int responseCode = con.getResponseCode();
      BufferedReader br;
      if(responseCode==200) { // 정상 호출
        br = new BufferedReader(new InputStreamReader(con.getInputStream()));
      } else {  // 오류 발생
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
      }
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = br.readLine()) != null) {
        response.append(inputLine);
      }
      br.close();
      System.out.println(response.toString());
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
