package com.example.ai.naver_clova.capcha;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CapchaAudio01Key {
  public static void main(String[] args) {
    // X-NCP-APIGW-API-KEY-ID
    String clientId = "wo0d8tt658";
    // X-NCP-APIGW-API-KEY
    String clientSecret = "1V59Y5TdzPBx2cIUHJaQ0MGDH9t3SsyFuijHtNHN";
    try {
      String code = "0"; // 키 발급시 0,  캡차 음성 비교시 1로 세팅
      String apiURL = "https://naveropenapi.apigw.ntruss.com/scaptcha/v1/skey?code=" + code;
      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection)url.openConnection();
      con.setRequestMethod("GET");
      con.setRequestProperty("X-NCP-APIGW-API-KEY-ID", clientId);
      con.setRequestProperty("X-NCP-APIGW-API-KEY", clientSecret);
      int responseCode = con.getResponseCode();
      BufferedReader br;
      if(responseCode==200) { // 정상 호출
        br = new BufferedReader(new InputStreamReader(con.getInputStream()));
      } else {  // 오류 발생
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
      }
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = br.readLine()) != null) {
        response.append(inputLine);
      }
      br.close();
      System.out.println(response.toString());
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
