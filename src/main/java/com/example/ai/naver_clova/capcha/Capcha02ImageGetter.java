package com.example.ai.naver_clova.capcha;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

public class Capcha02ImageGetter {
  public static void main(String[] args) {
    String clientId = "wo0d8tt658";
    try {
      String key = "YdySbsZkC1hhMYmz"; // https://naveropenapi.apigw.ntruss.com/captcha/v1/nkey 호출로 받은 키값
      String apiURL = "https://naveropenapi.apigw.ntruss.com/captcha-bin/v1/ncaptcha?key=" + key + "&X-NCP-APIGW-API-KEY-ID" + clientId;
      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection)url.openConnection();
      con.setRequestMethod("GET");
      int responseCode = con.getResponseCode();
      BufferedReader br;
      if(responseCode==200) { // 정상 호출
        InputStream is = con.getInputStream();
        int read = 0;
        byte[] bytes = new byte[1024];
        // 랜덤한 이름으로 파일 생성
        String tempname = Long.valueOf(new Date().getTime()).toString();
        File f = new File(tempname + ".jpg");
        f.createNewFile();
        OutputStream outputStream = new FileOutputStream(f);
        while ((read =is.read(bytes)) != -1) {
          outputStream.write(bytes, 0, read);
        }
        is.close();
      } else {  // 오류 발생
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = br.readLine()) != null) {
          response.append(inputLine);
        }
        br.close();
        System.out.println(response.toString());
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
