package com.example.ai.naver_clova.voice;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class STTAPIDemo {
  public static void main(String[] args) {
    // X-NCP-APIGW-API-KEY-ID
    String clientId = "wo0d8tt658";
    // X-NCP-APIGW-API-KEY
    String clientSecret = "1V59Y5TdzPBx2cIUHJaQ0MGDH9t3SsyFuijHtNHN";

    String lang = "Kor";
    // 요악을 해줄 API
    String apiURL = "https://naveropenapi.apigw.ntruss.com/recog/v1/stt?lang=" + lang;

    String voiceFilePath = "src/main/java/com/example/ai/naver_clova/voice/set001010.wav";

    try {
      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setDoOutput(true);
      con.setRequestMethod("POST");

      // 인증키 입력하는 부분
      con.setRequestProperty("X-NCP-APIGW-API-KEY-ID", clientId);
      con.setRequestProperty("X-NCP-APIGW-API-KEY", clientSecret);

      con.setRequestProperty("Content-Type", "application/octet-stream");

      // 요청할 내용 입력하는 부분
      File voiceFile = new File(voiceFilePath);
      FileInputStream inputStream = new FileInputStream(voiceFile);
      OutputStream outputStream = con.getOutputStream();
      byte[] buffer = new byte[4096];
      int bytesRead = -1;
      while ((bytesRead = inputStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, bytesRead);
      }
      outputStream.flush();
      inputStream.close();

      int responseCode = con.getResponseCode();
      BufferedReader br;
      if (responseCode == 200) {
        br = new BufferedReader(new InputStreamReader(con.getInputStream()));
      } else {
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
      }
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = br.readLine()) != null) {
        response.append(inputLine);
      }
      br.close();

      String jsonResult = response.toString();

      System.out.println(jsonResult);
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
