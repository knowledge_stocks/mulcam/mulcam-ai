package com.example.ai.naver_clova.voice.servlet;

import com.jayway.jsonpath.JsonPath;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@MultipartConfig(
    location = "D:\\Users\\admin\\Dev\\Repos\\For Career\\Study\\Mulcam\\AI\\mulcam-ai\\target\\cargo\\upload\\temp",
    fileSizeThreshold = 1024*1024, // 1MB보다 작으면 메모리에, 보다 크면 location에 지정한 경로에 파일로
    maxFileSize = 1024*1024*50, // 파일 하나하나의 최대 사이즈, 각 파일은 50MB를 넘을 수 없다.
    maxRequestSize = 1024*1024*50*5 // 전체 리퀘스트의 최대 사이즈, 모든 파일의 합은 250MB를 넘을 수 없다.
)
@WebServlet("/upload")
public class SpeechSTTProcess extends HttpServlet {
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String saveDirectory = "D:\\Users\\admin\\Dev\\Repos\\For Career\\Study\\Mulcam\\AI\\mulcam-ai\\target\\cargo\\upload\\temp";
    File tempDir = new File(saveDirectory);
    if(!tempDir.exists()) {
      tempDir.mkdirs();
    }

    // X-NCP-APIGW-API-KEY-ID
    String clientId = "wo0d8tt658";
    // X-NCP-APIGW-API-KEY
    String clientSecret = "1V59Y5TdzPBx2cIUHJaQ0MGDH9t3SsyFuijHtNHN";

    String lang = "Kor";
    // 요악을 해줄 API
    String apiURL = "https://naveropenapi.apigw.ntruss.com/recog/v1/stt?lang=" + lang;

    try {
      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setDoOutput(true);
      con.setRequestMethod("POST");

      // 인증키 입력하는 부분
      con.setRequestProperty("X-NCP-APIGW-API-KEY-ID", clientId);
      con.setRequestProperty("X-NCP-APIGW-API-KEY", clientSecret);

      con.setRequestProperty("Content-Type", "application/octet-stream");

      // 요청할 내용 입력하는 부분
      Part part = req.getPart("data");
      InputStream inputStream = part.getInputStream();
      OutputStream outputStream = con.getOutputStream();
      byte[] buffer = new byte[4096];
      int bytesRead = -1;
      while ((bytesRead = inputStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, bytesRead);
      }
      outputStream.flush();
      inputStream.close();

      int responseCode = con.getResponseCode();
      BufferedReader br;
      if (responseCode == 200) {
        br = new BufferedReader(new InputStreamReader(con.getInputStream()));
      } else {
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
      }
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = br.readLine()) != null) {
        response.append(inputLine);
      }
      br.close();

      String jsonResult = response.toString();
      System.out.println(jsonResult);

      String text = JsonPath.read(jsonResult, "$.text");
      resp.setContentType("application/json; charset=utf-8");
      resp.getWriter().print(text);
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
