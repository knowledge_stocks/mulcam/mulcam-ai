package com.example.ai.naver_clova.ocr;

import com.google.gson.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.GsonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.GsonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.EnumSet;
import java.util.Set;
import java.util.UUID;

public class OCRTemplateAPIDemo_02 {
  public static void main(String[] args) {
    Configuration.setDefaults(new Configuration.Defaults() {
      private final JsonProvider jsonProvider = new GsonJsonProvider();
      private final MappingProvider mappingProvider = new GsonMappingProvider();

      @Override
      public JsonProvider jsonProvider() {
        return jsonProvider;
      }

      @Override
      public MappingProvider mappingProvider() {
        return mappingProvider;
      }

      @Override
      public Set<Option> options() {
        return EnumSet.noneOf(Option.class);
      }
    });

    String apiURL = "https://58597ada48334e3a8e928e868bc8a7c4.apigw.ntruss.com/custom/v1/13667/7477115cf8578747bc76dc88b138370076706d8b90805f1ef21a4dcbbdeaf218/infer";
    String secretKey = "V1NiTXR5TkJEbnl4SkxDZ2htbXZoS21VTHpsYnB4RmQ=";
    String imageFile = "src/main/java/com/example/ai/naver_clova/ocr/template/corporate_2.jpg";

    try {
      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setUseCaches(false);
      con.setDoInput(true);
      con.setDoOutput(true);
      con.setReadTimeout(30000);
      con.setRequestMethod("POST");
      String boundary = "----" + UUID.randomUUID().toString().replaceAll("-", "");
      con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
      con.setRequestProperty("X-OCR-SECRET", secretKey);

      JsonObject json = new JsonObject();
      json.addProperty("version", "V1");
      json.addProperty("requestId", UUID.randomUUID().toString());
      json.addProperty("timestamp", System.currentTimeMillis());
      JsonObject image = new JsonObject();
      image.addProperty("format", "jpg");
      image.addProperty("name", "demo");
      JsonArray images = new JsonArray();
      images.add(image);
      json.add("images", images);
      String postParams = json.toString();

      con.connect();
      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
      long start = System.currentTimeMillis();
      File file = new File(imageFile);
      writeMultiPart(wr, postParams, file, boundary);
      wr.close();

      int responseCode = con.getResponseCode();
      BufferedReader br;
      if (responseCode == 200) {
        br = new BufferedReader(new InputStreamReader(con.getInputStream()));
      } else {
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
      }
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = br.readLine()) != null) {
        response.append(inputLine);
      }
      br.close();

      String jsonResult = response.toString();

      System.out.println(jsonResult);
      getOnlyInferTextUsingJsonPath(jsonResult);
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  private static void getOnlyInferTextUsingJsonPath(String jsonResult) {
    String title = ((JsonElement)JsonPath.read(jsonResult, "$.images[0].title.inferText")).getAsString();

    System.out.println("title: " + title);

    JsonArray inferFields = JsonPath.read(jsonResult, "$.images[0].fields");
    for(JsonElement e : inferFields) {
      String name = ((JsonElement) JsonPath.read(e.toString(), "$.name")).getAsString();
      String inferText = ((JsonElement) JsonPath.read(e.toString(), "$.inferText")).getAsString();
      System.out.println(name + ": " + inferText);
    }
  }

  private static void writeMultiPart(OutputStream out, String jsonMessage, File file, String boundary) throws
      IOException {
    StringBuilder sb = new StringBuilder();
    sb.append("--").append(boundary).append("\r\n");
    sb.append("Content-Disposition:form-data; name=\"message\"\r\n\r\n");
    sb.append(jsonMessage);
    sb.append("\r\n");

    out.write(sb.toString().getBytes("UTF-8"));
    out.flush();

    if (file != null && file.isFile()) {
      out.write(("--" + boundary + "\r\n").getBytes("UTF-8"));
      StringBuilder fileString = new StringBuilder();
      fileString
          .append("Content-Disposition:form-data; name=\"file\"; filename=");
      fileString.append("\"" + file.getName() + "\"\r\n");
      fileString.append("Content-Type: application/octet-stream\r\n\r\n");
      out.write(fileString.toString().getBytes("UTF-8"));
      out.flush();

      try (FileInputStream fis = new FileInputStream(file)) {
        byte[] buffer = new byte[8192];
        int count;
        while ((count = fis.read(buffer)) != -1) {
          out.write(buffer, 0, count);
        }
        out.write("\r\n".getBytes());
      }

      out.write(("--" + boundary + "--\r\n").getBytes("UTF-8"));
    }
    out.flush();
  }
}
