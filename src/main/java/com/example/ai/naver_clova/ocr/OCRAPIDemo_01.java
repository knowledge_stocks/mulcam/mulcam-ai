package com.example.ai.naver_clova.ocr;

import com.google.gson.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.GsonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.GsonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.EnumSet;
import java.util.Set;
import java.util.UUID;

public class OCRAPIDemo_01 {
  public static void main(String[] args) {
    Configuration.setDefaults(new Configuration.Defaults() {
      private final JsonProvider jsonProvider = new GsonJsonProvider();
      private final MappingProvider mappingProvider = new GsonMappingProvider();

      @Override
      public JsonProvider jsonProvider() {
        return jsonProvider;
      }

      @Override
      public MappingProvider mappingProvider() {
        return mappingProvider;
      }

      @Override
      public Set<Option> options() {
        return EnumSet.noneOf(Option.class);
      }
    });

    String apiURL = "https://58597ada48334e3a8e928e868bc8a7c4.apigw.ntruss.com/custom/v1/13642/4e11d577d36e728534b4bcd2c551b53d6914b2fb0b10c18e0d2cfdfee13dd347/general";
    String secretKey = "TUpTWEdYRW9DT0ZZcVBsWVhBcHpBZG56TUN6QnBsYnI=";
    String imageFile = "src/main/java/com/example/ai/naver_clova/ocr/baby_shark.jpg";

    try {
      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setUseCaches(false);
      con.setDoInput(true);
      con.setDoOutput(true);
      con.setReadTimeout(30000);
      con.setRequestMethod("POST");
      String boundary = "----" + UUID.randomUUID().toString().replaceAll("-", "");
      con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
      con.setRequestProperty("X-OCR-SECRET", secretKey);

      JsonObject json = new JsonObject();
      json.addProperty("version", "V2");
      json.addProperty("requestId", UUID.randomUUID().toString());
      json.addProperty("timestamp", System.currentTimeMillis());
      JsonObject image = new JsonObject();
      image.addProperty("format", "jpg");
      image.addProperty("name", "demo");
      JsonArray images = new JsonArray();
      images.add(image);
      json.add("images", images);
      String postParams = json.toString();

      con.connect();
      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
      long start = System.currentTimeMillis();
      File file = new File(imageFile);
      writeMultiPart(wr, postParams, file, boundary);
      wr.close();

      int responseCode = con.getResponseCode();
      BufferedReader br;
      if (responseCode == 200) {
        br = new BufferedReader(new InputStreamReader(con.getInputStream()));
      } else {
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
      }
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = br.readLine()) != null) {
        response.append(inputLine);
      }
      br.close();

      String jsonResult = response.toString();

      System.out.println(jsonResult);
      getOnlyInferText1(jsonResult);
      getOnlyInferTextUsingJsonPath(jsonResult);
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  private static void getOnlyInferText1(String jsonResult) {
    JsonObject obj = null;
    try {
      obj = (JsonObject) JsonParser.parseString(jsonResult);
      JsonArray images = (JsonArray) obj.get("images");
      JsonObject dataObj = (JsonObject) images.get(0);
      JsonArray fields = (JsonArray) dataObj.get("fields");

      StringBuffer result = new StringBuffer();
      for(int i = 0; i < fields.size(); i++) {
        JsonObject item = (JsonObject) fields.get(i);
        result.append(item.get("inferText").getAsString() + " ");
      }
      System.out.println(result);
    } catch (JsonSyntaxException e) {
      e.printStackTrace();
    }
  }

  private static void getOnlyInferTextUsingJsonPath(String jsonResult) {
    JsonArray inferTextList = JsonPath.read(jsonResult, "$.images[0].fields[*].inferText");
//    System.out.println(String.join(" ", inferTextList));
    StringBuffer result = new StringBuffer();
    for(JsonElement e : inferTextList) {
      result.append(e.getAsString() + " ");
    }
    System.out.println(result);
  }

  private static void writeMultiPart(OutputStream out, String jsonMessage, File file, String boundary) throws
      IOException {
    StringBuilder sb = new StringBuilder();
    sb.append("--").append(boundary).append("\r\n");
    sb.append("Content-Disposition:form-data; name=\"message\"\r\n\r\n");
    sb.append(jsonMessage);
    sb.append("\r\n");

    out.write(sb.toString().getBytes("UTF-8"));
    out.flush();

    if (file != null && file.isFile()) {
      out.write(("--" + boundary + "\r\n").getBytes("UTF-8"));
      StringBuilder fileString = new StringBuilder();
      fileString
          .append("Content-Disposition:form-data; name=\"file\"; filename=");
      fileString.append("\"" + file.getName() + "\"\r\n");
      fileString.append("Content-Type: application/octet-stream\r\n\r\n");
      out.write(fileString.toString().getBytes("UTF-8"));
      out.flush();

      try (FileInputStream fis = new FileInputStream(file)) {
        byte[] buffer = new byte[8192];
        int count;
        while ((count = fis.read(buffer)) != -1) {
          out.write(buffer, 0, count);
        }
        out.write("\r\n".getBytes());
      }

      out.write(("--" + boundary + "--\r\n").getBytes("UTF-8"));
    }
    out.flush();
  }
}
