package com.example.ai.naver_clova.shorturl;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ShortUrl {
  public static void main(String[] args) {
    // X-NCP-APIGW-API-KEY-ID
    String clientId = "wo0d8tt658";
    // X-NCP-APIGW-API-KEY
    String clientSecret = "1V59Y5TdzPBx2cIUHJaQ0MGDH9t3SsyFuijHtNHN";
    try {
      String text = "https://developers.naver.com/notice";
      String apiURL = "https://naveropenapi.apigw.ntruss.com/util/v1/shorturl";
      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection)url.openConnection();
      con.setDoOutput(true);
      con.setRequestMethod("POST");
      con.setRequestProperty("Content-Type", "application/json");
      con.setRequestProperty("X-NCP-APIGW-API-KEY-ID", clientId);
      con.setRequestProperty("X-NCP-APIGW-API-KEY", clientSecret);
      // post request
      JSONObject json = new JSONObject();
      json.put("url", text);
      String postParams = json.toString();
      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
      wr.writeBytes(postParams);
      wr.flush();
      wr.close();
      int responseCode = con.getResponseCode();
      BufferedReader br;
      if(responseCode==200) { // 정상 호출
        br = new BufferedReader(new InputStreamReader(con.getInputStream()));
      } else {  // 오류 발생
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
      }
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = br.readLine()) != null) {
        response.append(inputLine);
      }
      br.close();
      System.out.println(response.toString());
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
