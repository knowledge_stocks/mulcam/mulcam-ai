package com.example.ai.spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

@Controller
public class AIController {
  @RequestMapping(value = "/map", method = RequestMethod.GET)
  public String map() {
    return "map";
  }

  @RequestMapping(value = "/staticmap", method = RequestMethod.GET)
  public String staticmap() {
    return "staticmap";
  }

  @RequestMapping(value = "/ocr", method = RequestMethod.GET)
  public String ocr() {
    return "ocr";
  }

  @RequestMapping(value = "/tts", method = RequestMethod.GET)
  public String tts() {
    return "tts";
  }

  @RequestMapping(value = "/tts", method = RequestMethod.POST)
  public void postTTS(@RequestParam("text") String inputText, HttpServletResponse resp) {
    // X-NCP-APIGW-API-KEY-ID
    String clientId = "wo0d8tt658";
    // X-NCP-APIGW-API-KEY
    String clientSecret = "1V59Y5TdzPBx2cIUHJaQ0MGDH9t3SsyFuijHtNHN";

    try {
      String text = URLEncoder.encode(inputText, "UTF-8");
      String apiURL = "https://naveropenapi.apigw.ntruss.com/tts-premium/v1/tts";
      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection)url.openConnection();
      con.setRequestMethod("POST");
      con.setRequestProperty("X-NCP-APIGW-API-KEY-ID", clientId);
      con.setRequestProperty("X-NCP-APIGW-API-KEY", clientSecret);
      // post request
      String postParams = "speaker=nara&volume=0&speed=0&pitch=0&format=mp3&text=" + text;
      con.setDoOutput(true);
      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
      wr.writeBytes(postParams);
      wr.flush();
      wr.close();
      int responseCode = con.getResponseCode();
      BufferedReader br;
      if(responseCode==200) { // 정상 호출
        InputStream is = con.getInputStream();
        int read = 0;
        byte[] bytes = new byte[1024];

        OutputStream out = resp.getOutputStream();

        resp.setContentType("audio/mpeg");
        resp.setHeader("Content-Disposition", "attachment; filename=\"" + "tts.mp3" + "\"");

        byte[] arrBuf = new byte[8192];
        int n;

        int contentLength = 0;
        while (true) {
          n = is.read(arrBuf);
          if (n <= 0) break;
          contentLength += n;
          out.write(arrBuf, 0, n);
        }
        is.close();
      } else {  // 오류 발생
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = br.readLine()) != null) {
          response.append(inputLine);
        }
        br.close();
        System.out.println(response.toString());
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  @RequestMapping(value = "/ttvoice", method = RequestMethod.GET)
  public String ttvoice() {
    return "speakertest";
  }

  @RequestMapping(value = "/ttvoice", method = RequestMethod.POST)
  public void postTTvoice(@RequestParam("textdata") String inputText, HttpServletResponse resp) {
    // X-NCP-APIGW-API-KEY-ID
    String clientId = "wo0d8tt658";
    // X-NCP-APIGW-API-KEY
    String clientSecret = "1V59Y5TdzPBx2cIUHJaQ0MGDH9t3SsyFuijHtNHN";

    try {
      String text = URLEncoder.encode(inputText, "UTF-8");
      String apiURL = "https://naveropenapi.apigw.ntruss.com/tts-premium/v1/tts";
      URL url = new URL(apiURL);
      HttpURLConnection con = (HttpURLConnection)url.openConnection();
      con.setRequestMethod("POST");
      con.setRequestProperty("X-NCP-APIGW-API-KEY-ID", clientId);
      con.setRequestProperty("X-NCP-APIGW-API-KEY", clientSecret);
      // post request
      String postParams = "speaker=nara&volume=0&speed=0&pitch=0&format=mp3&text=" + text;
      con.setDoOutput(true);
      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
      wr.writeBytes(postParams);
      wr.flush();
      wr.close();
      int responseCode = con.getResponseCode();
      BufferedReader br;
      if(responseCode==200) { // 정상 호출
        InputStream is = con.getInputStream();
        int read = 0;
        byte[] bytes = new byte[1024];

        OutputStream out = resp.getOutputStream();

        resp.setContentType("audio/mpeg");
        resp.setHeader("Content-Disposition", "attachment; filename=\"" + "tts.mp3" + "\"");

        byte[] arrBuf = new byte[8192];
        int n;

        int contentLength = 0;
        while (true) {
          n = is.read(arrBuf);
          if (n <= 0) break;
          contentLength += n;
          out.write(arrBuf, 0, n);
        }
        is.close();
      } else {  // 오류 발생
        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = br.readLine()) != null) {
          response.append(inputLine);
        }
        br.close();
        System.out.println(response.toString());
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
