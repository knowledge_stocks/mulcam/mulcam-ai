package com.example.ai.kakao.map.service;

import com.example.ai.kakao.map.dao.LibmapDAO;
import com.example.ai.kakao.map.dto.LibmapDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class LibmapServiceImpl implements LibmapService {
  @Autowired
  LibmapDAO libmapDAO;

  @Override
  public List<LibmapDTO> f_listProcess(int pageNo, int pageSize, String keyword) {
    HashMap<String, Object> map = new HashMap<String, Object>();
    map.put("startpage", ((pageNo-1) * pageSize));
    map.put("endpage", pageSize);
    map.put("keyword", keyword);
    return libmapDAO.lib_list(map);
  }

  @Override
  public int f_countAllProcess(String data) {
    return libmapDAO.lib_countAll(data);
  }
}
