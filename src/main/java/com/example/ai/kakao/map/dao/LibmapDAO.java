package com.example.ai.kakao.map.dao;


import com.example.ai.kakao.map.dto.LibmapDTO;

import java.util.HashMap;
import java.util.List;

public interface LibmapDAO {
  public List<LibmapDTO> lib_list(HashMap<String, Object> map);

  public int lib_countAll(String data);
}
