package com.example.ai.kakao.map.controller;

import com.example.ai.kakao.map.dto.LibmapDTO;
import com.example.ai.kakao.map.service.LibmapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
public class LibmapController {
  @Autowired
  LibmapService libmapService;

  @RequestMapping(value = "/kakao/map", method = RequestMethod.GET)
  public String mapForm() {
    return "kakao/map";
  }

  @RequestMapping(value = "kakao/map/search", method = RequestMethod.POST)
  @ResponseBody
  public HashMap<String, Object> mapSearch(
      @RequestParam("pageNo") int pageNo,
      @RequestParam("pageSize") int pageSize,
      @RequestParam("keyword") String keyword
  ) {
    int countAll = libmapService.f_countAllProcess(keyword);

    int totalPage = countAll/pageSize;
    if((countAll%pageSize) > 0) {
      totalPage++;
    }

    List<LibmapDTO> result = libmapService.f_listProcess(pageNo, pageSize, keyword);

    HashMap<String, Object> map = new HashMap<String, Object>();
    map.put("aList", result);
    map.put("pageNo", pageNo);
    map.put("totalPage", totalPage);

    return map;
  }
}
