package com.example.ai.kakao.map.dao;

import com.example.ai.kakao.map.dto.LibmapDTO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class LibmapDAOImpl implements LibmapDAO {
  @Autowired
  private SqlSessionTemplate sqlSession;

  @Override
  public List<LibmapDTO> lib_list(HashMap<String, Object> map) {
    return sqlSession.selectList("lib.lib_list", map);
  }

  @Override
  public int lib_countAll(String data) {
    return sqlSession.selectOne("lib.lib_countAll", data);
  }
}
