package com.example.ai.kakao.map.service;

import com.example.ai.kakao.map.dto.LibmapDTO;

import java.util.List;

public interface LibmapService {
  public List<LibmapDTO> f_listProcess(
      int pageNo, int pageSize,
      String keyword);

  public int f_countAllProcess(String data);
}