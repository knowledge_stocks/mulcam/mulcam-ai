package com.example.ai.kakao.map.dto;

import lombok.Data;

@Data
public class LibmapDTO {
  private String mart;
  private String p_num;
  private String area;
  private String address;
  private String latitude;
  private String longitude;
}
